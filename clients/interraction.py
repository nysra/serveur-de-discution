from threading import Thread

def connection(serveur):
    """[fonction qui renvoit l'état de la connection au serveur apres s'être tenté de se connecter
     et  le nom de compte]
    
    Arguments:
        serveur {[socket]} -- [la connection au serveur]
    
    Returns:
        [tuple(str,str)] -- [l'etat de la connection (reussie ou pas) et le nom de compte]
    """


    print(serveur.recv(1024).decode()) # on demande de rentrer le nom de compte
    nom_de_compte=input().encode()
    serveur.send(nom_de_compte)

    connection_mdp=serveur.recv(1024).decode() # on enregistre l'instruction donnée en fonction du nom de compte

    if connection_mdp=="donnez votre mot de passe": #si le compte existe on demande le mot de passe
        print(connection_mdp)   
        mot_de_passe=input()
        serveur.send(mot_de_passe.encode())
    elif connection_mdp=="bonjour {} quel mot de passe choisisez vous? (Plus de 5 carateres)".format(nom_de_compte.decode()):
        création_de_compte(serveur,nom_de_compte,connection_mdp)
    elif connection_mdp=="Le compte est deja connecte":
        print(connection_mdp)
    etat_connection=serveur.recv(1024).decode()
    return etat_connection, nom_de_compte

def verification_compte(serveur):
    print(serveur.recv(1024).decode()) # on demande de rentrer le nom de compte
    nom_de_compte=input().encode()
    serveur.send(nom_de_compte)

    connection_mdp=serveur.recv(1024).decode() # on enregistre l'instruction donnée en fonction du nom de compte

    if connection_mdp=="donnez votre mot de passe": #si le compte existe on demande le mot de passe
        print(connection_mdp)   
        mot_de_passe=input()
        serveur.send(mot_de_passe.encode())
    if connection_mdp=="bonjour {} quel mot de passe choisisez vous? (Plus de 5 carateres)".format(nom_de_compte.decode()):
        fini=False          #si il n'existe pas on demande de creer un mot de passe juqu'a ce qu'il soit valide
        while not fini:
            création_de_compte(serveur,nom_de_compte,connection_mdp)
    etat_connection=serveur.recv(1024).decode()

def création_de_compte(serveur,nom_de_compte,message):
    mot_de_passe=""
    while len(mot_de_passe)<5:
            print(message)
            mot_de_passe=input()
            serveur.send(mot_de_passe.encode())
    return


def envoyer_message(nom_de_compte,message,serveur):
    serveur.send(message)
    return

def recevoir_messages(serveur,messages_deja_lu):
    messages=[]
    continu="ok"
    nombre_messages=serveur.recv(1024)
    for a in range(int(nombre_messages)):
        serveur.send(continu.encode())
        msg_lu=serveur.recv(1024)
        print(msg_lu.decode())
    return nombre_messages

