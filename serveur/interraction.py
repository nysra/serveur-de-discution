import pickle
from threading import Thread
def verification_compte(client,fichier_compte,info_clients):
    with open(f"{fichier_compte}.txt","rb") as fichier:
        liste_comptes=pickle.Unpickler(fichier).load()
    client.send(b"donnez votre nom de compte")
    nom_de_compte=client.recv(1024).decode()
    for deja_co in info_clients.values():
        if nom_de_compte== deja_co[0]:
            return False , b"Le compte est deja connecte" , nom_de_compte
    if len(liste_comptes)>0: #si on a au moins un compte
        compte_exite=False
        for cle in liste_comptes.keys():
            if nom_de_compte ==cle: #on verifie si le compte existe
                compte_exite=True
                client.send(b"donnez votre mot de passe")#si oui on demande le mots de passe
                if client.recv(1024).decode()==liste_comptes[nom_de_compte]:#si le mots de passe correspond a celui enregistré
                    return True , b"Bienvenue" , nom_de_compte
                else: 
                    return False , b"mauvais mot de passe veuillez reessayer", nom_de_compte
        if not compte_exite:
            if creation_de_compte(nom_de_compte,client,fichier_compte):
                return True , b"Bienvenue" , nom_de_compte
            return False , b"le nom de compte existe deja" ,nom_de_compte
    else:
        if creation_de_compte(nom_de_compte,client,fichier_compte):
            return True , b"Bienvenue" ,nom_de_compte
        return False , b"le nom de compte existe deja"

def creation_de_compte(nom_de_compte,client,fichier_compte):
    client.send("bonjour {} quel mot de passe choisisez vous? (Plus de 5 carateres)".format(nom_de_compte).encode())
    with open(f"{fichier_compte}.txt","rb") as fichier:
        liste_comptes=pickle.Unpickler(fichier).load()
    while True:
        mot_de_passe=""
        while len(mot_de_passe)<5:
            mot_de_passe=client.recv(1024).decode()
        liste_comptes[nom_de_compte]=mot_de_passe
        with open(f"{fichier_compte}.txt","wb") as fichier: # on enregistre la liste des comptes dans un fichier
            pickle.Pickler(fichier).dump(liste_comptes)
        return True

def recevoir_message(clients,info_clients):
    nom_de_compte=info_clients[clients][0]
    msg_recu=clients.recv(1024,).decode()
    return [nom_de_compte," : ",msg_recu]

def envoyer_message(clients,liste_messages,info_clients):
    nom_de_compte=info_clients[clients][0]
    deja_envoye=info_clients[clients][1]
    liste_a_envoyer=liste_messages[deja_envoye:len(liste_messages)]
    deja_envoye+=len(liste_a_envoyer)
    for elemnts in liste_a_envoyer:
        if elemnts[0] == nom_de_compte:
            liste_a_envoyer.remove(elemnts)
    nombre_messages=len(liste_a_envoyer)
    clients.send(str(nombre_messages).encode())
    for elements in liste_a_envoyer:
        continu=clients.recv(1024)
        clients.send("".join(elements).encode())
    return deja_envoye


def connection(connexion,clients_connectes,info_clients,clients_en_attente,messages,client_en_interraction):
    print("a")
    connection_reussie,message,nom_de_compte=verification_compte(connexion,"mdps",info_clients)
    if connection_reussie :
        clients_connectes.append(connexion) # on le connecte et on l'enleve de la file d'attente
        clients_en_attente.remove(connexion)
        connexion.send(message)
        info_clients[connexion]=[nom_de_compte,len(messages)]
    else:
        connexion.send(message) #sinon on l'enleve juste de la file d'attente
        clients_en_attente.remove(connexion)
        connexion.close()
    client_en_interraction.remove(connexion)

def tchat(clients,clients_connectes,info_clients,messages,client_en_interraction):
    msg_recu=recevoir_message(clients,info_clients)
    if msg_recu[2]=="fin":
        clients_connectes.remove(clients)
        del info_clients[clients]
        clients.close()
        
    else :
        if msg_recu[2]!=" ":
            messages.append(msg_recu)
        info_clients[clients][1]=envoyer_message(clients,messages,info_clients)
    client_en_interraction.remove(clients)
