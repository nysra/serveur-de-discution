import socket
import select
import pickle
import time
from threading import Thread
from serveur.interraction import *
global nb_messages_envoye
nb_messages_envoye={}
hote = 'localhost'
port = 12800

connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connexion_principale.bind((hote, port))
connexion_principale.listen(5)
print("Le serveur écoute à présent sur le port {}".format(port))

serveur_lance=True

clients_en_attente=[]
clients_connectes=[]
messages=[]
info_clients={}
client_en_interraction=[]

while serveur_lance:
    connexions_demandees,wlist,xlist=select.select([connexion_principale],[],[],0.05)
    for connexion in connexions_demandees:
        if connexion not in clients_connectes: #si notre nouvelle connection n'est pas deja connecté on l'ajoute a la fille d'attente
            connexion_avec_clients,infos_connexion=connexion.accept()
            clients_en_attente.append(connexion_avec_clients)
    for connexion in clients_en_attente:  #on demande a chaque connection de ne connecter
        if connexion not in client_en_interraction:
            client_en_interraction.append(connexion)
            Thread(target=connection,args=(connexion,clients_connectes,info_clients,clients_en_attente,messages,client_en_interraction)).start()
            

    for clients in clients_connectes:
        if clients not in client_en_interraction:
            client_en_interraction.append(clients)
            Thread(target=tchat,args=(clients,clients_connectes,info_clients,messages,client_en_interraction)).start()

        
#print("Fermeture des connexions")
#for client in clients_connectes:
#    client.close()

connexion_principale.close()
